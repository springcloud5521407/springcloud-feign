package com.qytest.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author qy
 * @date 2022年07月15日 13:45
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer1Application {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer1Application.class);
    }
}